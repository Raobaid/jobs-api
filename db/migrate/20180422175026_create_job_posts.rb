class CreateJobPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :job_posts do |t|
      t.string :title,              null: false, default: ""
      t.string :description,       null: false, default: ""
      t.timestamps
    end
  end
end
