class CreateJobApplications < ActiveRecord::Migration[5.1]
  def change
    create_table :job_applications do |t|
      t.string :user_id,              null: false, default: ""
      t.string :job_post_id,          null: false, default: ""
      t.boolean :seen,                :default => false
      t.timestamps
    end
  end
end
