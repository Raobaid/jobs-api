class V1::UsersController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    if !User.exists?(email: params[:email]) # user already in database
      obj = User.new(email: params[:email], password: params[:password])
      obj.save
      render json: obj.as_json(only: [:email, :authentication_token, :admin]),  status: :created
    else
      head 409
    end
  end

end
