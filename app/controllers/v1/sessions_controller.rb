class V1::SessionsController < ApplicationController
  skip_before_action :verify_authenticity_token
  # before_action :create

  def create
    #log in user
    user = User.where(email: params[:email]).first
    # check user exits and passwords match
      if user && user.valid_password?(params[:password])
        render json: user.as_json(only: [:email, :authentication_token, :admin]),  status: :created
      else
        head 401
      end
  end

  def create_job_post
    if current_user.admin?
      obj = JobPost.new(title: params[:title], description: params[:description])
      obj.save
      render json: obj.as_json(only: [:id, :title, :description]),  status: :created
    else
      head 401
    end
  end

  def delete_job_post
    if current_user.admin?
      obj = JobPost.where(id: params[:id]).first
      if obj
        obj.destroy
        head 200
      else
        head 409 #JobPost with that id was not found
      end
    else
      head 401 # not authorized
    end
  end

  def update_job_post
    if current_user.admin?
      obj = JobPost.where(id: params[:id])
      if obj
        obj.update_all(title: params[:title], description:params[:description])
        head 201 # updated
      else
        head 409 #JobPost with that id was not found
      end
    else
      head 401 # not authorized
    end
  end

  def list_job_post
    obj = JobPost.all
    # # I kept all data because for the job, its important to know when it was created and updated in view
    render json: {data:obj}, status: :ok
  end

  def list_job_application
    obj = JobApplication.all
    render json: {data:obj}, status: :ok
  end

  def create_job_application
    obj = JobApplication.where(user_id: params[:user_id], job_post_id: params[:post_id]).first # pair of key
    if  !current_user.admin? && !obj
      obj = JobApplication.new(user_id: params[:user_id], job_post_id: params[:post_id])
      obj.save
      render json: obj.as_json(only: [:id, :user_id, :job_post_id, :seen]),  status: :created
    else
      head 401 # unauthorized to create job application
    end
  end

  def view_application

    if current_user.admin?
      obj = JobApplication.where(id: params[:id]).first
      if obj
        obj.update(seen: 1) # just update application to seen, since viewed by admin
        head 201
      else
        head 409
      end
    else
      head 401
    end
  end

  def destroy # destory session
    session[:user_id] = nil
  end


end
