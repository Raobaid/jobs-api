Rails.application.routes.draw do
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  namespace :v1 do
    resources :sessions do
      collection do
        post 'create_job_post'
        delete 'delete_job_post'
        put 'update_job_post'
        get 'list_job_post'
        get 'list_job_application'
        post 'create_job_application'
        put 'view_application'
      end
    end
    resources :users
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
